FROM python:3-alpine

RUN python3 -m venv /app
RUN source /app/bin/activate && \
    pip3 install pymodbus==3.6.9 PyYAML influxdb solaredge_modbus paho-mqtt modbus-proxy
COPY modbus-config.yml solaredge.py run.sh /app/

CMD ["/app/run.sh"]
